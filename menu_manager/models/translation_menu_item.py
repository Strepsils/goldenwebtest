from django.db import models
from django.utils.translation import ugettext_lazy as _


class TranslationMenuItem(models.Model):
    
    language = models.ForeignKey(
        "menu_manager.Language",
        verbose_name=_("Language"),
        on_delete=models.CASCADE
    )
    menu_item = models.ForeignKey(
        "menu_manager.MenuItem",
        verbose_name=_("Menu item"),
        on_delete=models.CASCADE,
        related_name="translations"
    )
    translated_name = models.CharField(_("Translated name"), max_length=50)

    def __str__(self):
        return f'<Item: {self.menu_item}> Lang: {self.language.name}'

    class Meta:
        verbose_name = _("Menu item translation")
        verbose_name = _("Menu items translation")
