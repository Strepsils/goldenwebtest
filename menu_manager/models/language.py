from django.db import models
from django.utils.translation import ugettext_lazy as _


class Language(models.Model):

    name = models.CharField(_("Name"), max_length=50)
    code = models.CharField(_("Code"), max_length=5)

    def __str__(self):
        return f'{self.name}({self.code})'

    class Meta:
        verbose_name = _("Language")
        verbose_name_plural = _("Languages")
