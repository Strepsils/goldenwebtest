from django.db import models
from django.utils.translation import ugettext_lazy as _


class MenuItem(models.Model):
    '''
        Model present menu item, which can be subitem and has parent item
    '''
    is_root = models.BooleanField(_("Is root element"), default=False)  # If menu item is root component - will render as start point at menu, each such element is different menu
    parent_item = models.ForeignKey(
        "self", verbose_name=_("Parent item"),
        on_delete=models.CASCADE, related_name="child_items",
        null=True, blank=True
    )  # parent item will be rendered as menu and this item as child item
    name = models.CharField(_("Name"), max_length=50)
    link = models.URLField(_("Link"), max_length=200, null=True, blank=True)  # Ignored for items which has childs

    def get_some(self, request):
        return [request.get_full_path()]

    def __str__(self):
        return f'{self.name}//{_("Parent")}: {self.parent_item}'

    class Meta:
        verbose_name = _("Menu item")
        verbose_name_plural = _("Menu items")
