from django.contrib import admin
from menu_manager import models

# Register your models here.

admin.site.register([
    models.MenuItem,
    models.Language,
    models.TranslationMenuItem
])
