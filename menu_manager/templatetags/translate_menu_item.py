from django import template
from menu_manager import models


register = template.Library()


@register.simple_tag(takes_context=True)
def translate_menu_item(context, item_id, field):
    request = context['request']
    language_code = request.session.get('language', 'en-us')
    item: models.MenuItem = models.MenuItem.objects.filter(id=item_id).first()
    if item is None:
        return item_id
    '''
        Try to fetch item fields translation for current language
    '''
    translated: models.TranslationMenuItem = models.TranslationMenuItem.objects.filter(
        language__code=language_code,
        menu_item=item
    ).first()
    if translated is None:
        '''
            if not found just select field from instance in db
        '''
        return getattr(item, field)
    '''
        Else, if translation exists - select field translation
        For fetching translated fields via template tag,
        all fields in translation item model must starts with 'translated_'
        and endswith field name( in menu item model ) 
    '''
    return getattr(translated, f'translated_{field}')
