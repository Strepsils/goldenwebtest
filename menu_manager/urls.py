from django.urls import path
from menu_manager import views


urlpatterns = [
    path('language/change', views.change_language, name="change_language"),
    path('', views.index, name="index")
]
