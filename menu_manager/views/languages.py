from django.shortcuts import redirect
from django.http import HttpResponseBadRequest


def change_language(request):
    next_url = request.POST.get('next', '/')
    new_language = request.POST.get('new_language')
    if new_language is None:
        return HttpResponseBadRequest()
    
    request.session['language'] = new_language
    print(request.session)
    return redirect(next_url)
