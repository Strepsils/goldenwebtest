from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from menu_manager import models


def index(request):
    root_menu_items = models.MenuItem.objects.filter(is_root=True).all()
    languages = models.Language.objects.all()
    selected_language = request.session.get('language', 'en-us')
    return render(
        request, 'menu_manager/index.html',
        {
            'menu_list': root_menu_items,
            'languages': languages,
            'selected_language': selected_language
        }
    )
